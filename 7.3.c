#include<stdio.h>

void main()
{
	int a[1000][1000], b[1000][1000], c[1000][1000]={0}, d[1000][1000]={0};
	int p, q, z, r1, c1, r2, c2;
	printf("Please enter the number of rows & columns in Matrix 1: \n");
	printf("Please enter the number of rows & columns in Matrix 2: \n");
	scanf("%d%d", &r2, &c2);
	if(r1!=r2 || c1!=c2)
	{
		printf("ERROR");
		return;
	}
	else if(c1!=r2)
	{
		printf("ERROR");
		return;
	}
	else
	{
		printf("Please enter the elements of matrix 1: \n");
		for(p=0; p<r1; p++)
			for(q=0; q<c1; q++)
				scanf("%d", &a[p][q]);
		printf("Please enter the elements of matrix 2: \n");
		for(p=0; p<r2; p++)
			for(q=0; q<c2; q++)
				scanf("%d", &b[p][q]);

		for(p=0; p<r1; p++)
			for(q=0; q<c1; q++)
				c[p][q] = a[p][q] + b[p][q];
		printf("Matrix Addition:\n");
		for(p=0; p<r1; p++)
		{
			for(q=0; q<c1; q++)
				printf("%d ", c[p][q]);
			printf("\n");
		}

		for(p=0; p<r1; p++)
			for(q=0; q<c2; q++)
				for(z=0; z<r2; z++)
					d[p][q] += a[p][z]*b[z][q];
		printf("Matrix Multiplication:\n");
		for(p=0; p<r1; p++)
		{
			for(q=0; q<c2; q++)
				printf("%d ", d[p][q]);
			printf("\n");
		}
	}

}


